provider "aws" {
  region                  = "${var.aws_region}"
  shared_credentials_file = "/Users/deamu/.aws/credentials"
  profile                 = "proact"
}

# BASIC: Keys
resource "aws_key_pair" "host-key" {
  key_name   = "host-key"
  public_key = "${file("/Users/deamu/.ssh/host-key.pub")}"
}

# END: KEYS

# BEGIN: Instances
resource "aws_instance" "bhost" {
  ami           = "${data.aws_ami.ubuntu.id}"
  instance_type = "${var.bhost_inst_type}"
  subnet_id     = "${aws_subnet.subnet_for_bhost.id}"
  key_name      = "${var.default_keyname}"

  vpc_security_group_ids = [
    "${aws_security_group.sg_allow_internet.id}",
    "${aws_security_group.sg_bastion.id}",
  ]

  monitoring = false

  count = "${var.bhost_inst_count}"

  tags {
    Name        = "Bastion Host"
    Environment = "development"
  }
}

resource "aws_instance" "app" {
  ami           = "${data.aws_ami.ubuntu.id}"
  instance_type = "${var.app_inst_type}"
  subnet_id     = "${aws_subnet.subnet_for_app.id}"
  key_name      = "${var.default_keyname}"

  vpc_security_group_ids = [
    "${aws_security_group.sg_allow_internet.id}",
    "${aws_security_group.sg_bastion.id}",
    "${aws_security_group.sg_web2app.id}",
  ]

  monitoring = false

  count = "${var.app_inst_count}"

  user_data = <<-EOF
                #!/bin/bash
                apt-get update
                apt-get install nginx
                systemctl start nginx.service
                systemctl enable nginx.service
                EOF

  tags {
    Name        = "Application"
    Environment = "development"
  }
}

resource "aws_instance" "db" {
  ami           = "${data.aws_ami.ubuntu.id}"
  instance_type = "${var.db_inst_type}"
  subnet_id     = "${aws_subnet.subnet_for_db.id}"
  key_name      = "${var.default_keyname}"

  vpc_security_group_ids = [
    "${aws_security_group.sg_allow_internet.id}",
    "${aws_security_group.sg_app2db.id}",
    "${aws_security_group.sg_admin.id}",
  ]

  user_data = <<-EOF
                #!/bin/bash
                apt-get install -y mariadb-server.x86_64
                echo mysql soft nofile ${var.mysql_nofile} >> /etc/security/limits.d/10-mysql.conf
                echo mysql hard nofile ${var.mysql_nofile} >> /etc/security/limits.d/10-mysql.conf
                sed -i -e '2ibind_address=0.0.0.0' /etc/my.cnf
                service mariadb start
                EOF

  monitoring = false

  count = "${var.db_inst_count}"

  tags {
    Name        = "Database Host"
    Environment = "development"
  }
}

data "template_file" "app_payload" {
  template = "${file("user_data_app.tpl")}"

  vars = {
    db_private_addr  = "${aws_instance.db.private_ip}"
    db_password      = "${var.db_password}"
    db_port          = "${var.db_port}"
    app_private_addr = "${aws_instance.app.private_ip}"
  }
}

resource "aws_launch_configuration" "web_lc" {
  image_id      = "${data.aws_ami.ubuntu.id}"
  instance_type = "${var.web_inst_type}"

  security_groups = [
    "${aws_security_group.sg_admin.id}",
    "${aws_security_group.sg_allow_internet.id}",
    "${aws_security_group.sg_lb2app.id}",
  ]

  key_name = "${var.default_keyname}"

  user_data         = "${data.template_file.app_payload.rendered}"
  enable_monitoring = false

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "web_asg" {
  launch_configuration = "${aws_launch_configuration.web_lc.id}"
  min_size             = "${var.web_inst_min_count}"
  max_size             = "${var.web_inst_max_count}"

  vpc_zone_identifier = ["${aws_subnet.subnet_for_app.id}"]

  tag {
    key                 = "Name"
    value               = "web-asg"
    propagate_at_launch = true
  }

  load_balancers    = ["${aws_elb.frontend_lb.name}"]
  health_check_type = "ELB"
}

resource "aws_elb" "frontend_lb" {
  name = "frontendlb"

  security_groups = [
    "${aws_security_group.elb.id}",
  ]

  subnets = ["${aws_subnet.subnet_for_lb.id}"]

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    interval            = 8
    target              = "HTTP:${var.web_http_port}/"
  }

  listener {
    lb_port           = "${var.elb_listen_http_port}"
    lb_protocol       = "http"
    instance_port     = "${var.web_http_port}"
    instance_protocol = "http"
  }
}

# END: Instances

